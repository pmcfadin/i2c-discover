#include <Wire.h>

/*  Tool to scan and report on what i2c devices are connected.
 *  
 *  Addresses have been takem from this awesome compilation
 *  
 *  https://learn.adafruit.com/i2c-addresses/the-list
 *  
 */

const int address_low  = 1;
const int address_high = 127;

void setup() {
  Wire.begin();
 
  Serial.begin(9600);
  
  while (!Serial);

  Serial.println("\nI2C Discover");
  
}

void loop() {
  byte response;
  byte bus_address;
 
  Serial.println("Scanning...");

  for(bus_address = address_low; bus_address < address_high; bus_address++ ) {

    // Open a connection to see if anything is there
    Wire.beginTransmission(bus_address);

    // Capture repsonse by closing the connection
    response = Wire.endTransmission();

    // A response of 0 means all went well
    // Anything larger that 0 means something went wrong
    if (response == 0) {

      if (bus_address < 16) {        
        Serial.println(getDevice(bus_address));
      } else { 
        Serial.println("Address out of bounds");
      }
     
    }
    else if (response > 4) {
      Serial.print("Response recieved: ");
      Serial.print(response);
      Serial.print(" at address 0");
      Serial.println(bus_address,HEX);
    }    
  }
 
  Serial.println ("Hit any key to re-scan");

  // Infinite loop until somebody hits a key
  while(Serial.available() == 0){}

}

// Take a bus address and try to match with a known device. 
// This method gets a lot of attention for just how to do a 
// memory efficient lookup wuth an arduino. 
String getDevice(byte bus_address) {

  String response = "Unknown Device";
  String hexAddress = String(bus_address, HEX);
  
  hexAddress.toUpperCase();

  hexAddress = "0x" + hexAddress;

  if (hexAddress == "0x0E") {
    response = "  MAG3110 3-Axis Magnetometer (0x0E only) ";
  }   
  if (hexAddress == "0x11") {
    response = "  Si4713 FM Transmitter with RDS (0x11 or 0x63) ";
  }   
  if (hexAddress == "0x13") {
    response = "  VCNL40x0 proximity sensor (0x13 only) ";
  }   
  if (hexAddress == "0x18") {
    response = "  MCP9808 temp sensor (0x18 - 0x1F)\n     LIS3DH 3-axis accelerometer (0x18 or 0x19)\n      LSM303 Accelerometer & Magnetometer (0x19 for accelerometer and 0x1E for magnetometer)  ";
  }   
  if (hexAddress == "0x19") {
    response = "  MCP9808 temp sensor (0x18 - 0x1F)\n     LIS3DH 3-axis accelerometer (0x18 or 0x19)  ";
  }   
  if (hexAddress == "0x1A") {
    response = "  MCP9808 temp sensor (0x18 - 0x1F) ";
  }   
  if (hexAddress == "0x1B") {
    response = "  MCP9808 temp sensor (0x18 - 0x1F) ";
  }   
  if (hexAddress == "0x1C") {
    response = "  MCP9808 temp sensor (0x18 - 0x1F)\n     MMA845x 3-axis Accelerometer (0x1C or 0x1D)\n     FXOS8700 Accelerometer/Magnetometer (0x1C, 0x1D, 0x1E or 0x1F)\n      MMA7455L (0x1C or 0x1D) ";
  }   
  if (hexAddress == "0x1D") {
    response = "  MCP9808 temp sensor (0x18 - 0x1F)\n     MMA845x 3-axis Accelerometer (0x1C or 0x1D)\n     FXOS8700 Accelerometer/Magnetometer (0x1C, 0x1D, 0x1E or 0x1F)\n      LSM9DS0 9-axis IMU (0x1D or 0x1E for Accel/Mag, 0x6A or 0x6B for Gyro)\n      ADXL345 3-axis accelerometer (0x1D or 0x53)\n     MMA7455L (0x1C or 0x1D) ";
  }   
  if (hexAddress == "0x1E") {
    response = "  MCP9808 temp sensor (0x18 - 0x1F)\n     FXOS8700 Accelerometer/Magnetometer (0x1C, 0x1D, 0x1E or 0x1F)\n      LSM303 Accelerometer & Magnetometer (0x19 for accelerometer and 0x1E for magnetometer)\n      LSM9DS0 9-axis IMU (0x1D or 0x1E for Accel/Mag, 0x6A or 0x6B for Gyro)\n      HMC5883 Magnetometer (0x1E only)  ";
  }   
  if (hexAddress == "0x1F") {
    response = "  MCP9808 temp sensor (0x18 - 0x1F)\n     FXOS8700 Accelerometer/Magnetometer (0x1C, 0x1D, 0x1E or 0x1F)  ";
  }   
  if (hexAddress == "0x20") {
    response = "  FXAS21002 Gyroscope (0x20 or 0x21)\n      Chirp! Water sensor (0x20)\n      MCP23008 I2C GPIO expander (0x20 - 0x27)\n      MCP23017 I2C GPIO expander (0x20 - 0x27)  ";
  }   
  if (hexAddress == "0x21") {
    response = "  FXAS21002 Gyroscope (0x20 or 0x21)\n      MCP23008 I2C GPIO expander (0x20 - 0x27)\n      MCP23017 I2C GPIO expander (0x20 - 0x27)  ";
  }   
  if (hexAddress == "0x22") {
    response = "  MCP23008 I2C GPIO expander (0x20 - 0x27)\n      MCP23017 I2C GPIO expander (0x20 - 0x27)  ";
  }   
  if (hexAddress == "0x23") {
    response = "  MCP23008 I2C GPIO expander (0x20 - 0x27)\n      MCP23017 I2C GPIO expander (0x20 - 0x27)  ";
  }   
  if (hexAddress == "0x24") {
    response = "  MCP23008 I2C GPIO expander (0x20 - 0x27)\n      MCP23017 I2C GPIO expander (0x20 - 0x27)  ";
  }   
  if (hexAddress == "0x25") {
    response = "  MCP23008 I2C GPIO expander (0x20 - 0x27)\n      MCP23017 I2C GPIO expander (0x20 - 0x27)  ";
  }   
  if (hexAddress == "0x26") {
    response = "  MCP23008 I2C GPIO expander (0x20 - 0x27)\n      MCP23017 I2C GPIO expander (0x20 - 0x27)  ";
  }   
  if (hexAddress == "0x27") {
    response = "  MCP23008 I2C GPIO expander (0x20 - 0x27)\n      MCP23017 I2C GPIO expander (0x20 - 0x27)  ";
  }   
  if (hexAddress == "0x28") {
    response = "  BNO055 IMU (0x28 or 0x29)\n     CAP1188 8-channel Capacitive Touch (0x28 - 0x2D ) ";
  }   
  if (hexAddress == "0x29") {
    response = "  BNO055 IMU (0x28 or 0x29)\n     TCS34725 color sensor (0x29 only)\n     TSL2561 light sensor (0x29, 0x39 or 0x49)\n     TSL2591 light sensor (0x29 only)\n      VL53L0x ToF distance (0x29, software selectable)\n      VL6180X ToF sensor (0x29)\n     CAP1188 8-channel Capacitive Touch (0x28 - 0x2D ) ";
  }   
  if (hexAddress == "0x2A") {
    response = "  CAP1188 8-channel Capacitive Touch (0x28 - 0x2D ) ";
  }   
  if (hexAddress == "0x2B") {
    response = "  CAP1188 8-channel Capacitive Touch (0x28 - 0x2D ) ";
  }   
  if (hexAddress == "0x2C") {
    response = "  CAP1188 8-channel Capacitive Touch (0x28 - 0x2D ) ";
  }   
  if (hexAddress == "0x2D") {
    response = "  CAP1188 8-channel Capacitive Touch (0x28 - 0x2D ) ";
  }   
  if (hexAddress == "0x38") {
    response = "  VEML6070 UV Index (0x38 and 0x39)\n     FT6x06 Capacitive Touch Driver (0x38 only)  ";
  }   
  if (hexAddress == "0x39") {
    response = "  TSL2561 light sensor (0x29, 0x39 or 0x49)\n     VEML6070 UV Index (0x38 and 0x39)\n     APDS-9960 IR/Color/Proximity Sensor (0x39 only) ";
  }   
  if (hexAddress == "0x3C") {
    response = "  SSD1305 monochrome OLED (0x3C or 0x3D, hardware selectable on some displays with a solder connection)\n     SSD1306 monochrome OLED (0x3C or 0x3D, hardware selectable on some displays with a solder connection) ";
  }   
  if (hexAddress == "0x3D") {
    response = "  SSD1305 monochrome OLED (0x3C or 0x3D, hardware selectable on some displays with a solder connection)\n     SSD1306 monochrome OLED (0x3C or 0x3D, hardware selectable on some displays with a solder connection) ";
  }   
  if (hexAddress == "0x40") {
    response = "  Si7021 Humidity/Temp sensor (0x40 only)\n     HTU21D-F Humidity/Temp Sensor (0x40 only)\n     HDC1008 Humidity/Temp sensor (0x40, 0x41, 0x42 or 0x43)\n     TMP007 IR Temperature sensor (0x40 - 0x47)\n      TMP006 IR Temperature sensor (0x40 - 0x47)\n      PCA9685 16-channel PWM driver default address (0x40 - 0x7F)\n     INA219 High-Side DC Current/Voltage sensor (0x40 - 0x4F)  ";
  }   
  if (hexAddress == "0x41") {
    response = "  HDC1008 Humidity/Temp sensor (0x40, 0x41, 0x42 or 0x43)\n     TMP007 IR Temperature sensor (0x40 - 0x47)\n      TMP006 IR Temperature sensor (0x40 - 0x47)\n      INA219 High-Side DC Current/Voltage sensor (0x40 - 0x4F)\n      STMPE610/STMPE811 Resistive Touch controller (0x41 or 0x44) ";
  }   
  if (hexAddress == "0x42") {
    response = "  HDC1008 Humidity/Temp sensor (0x40, 0x41, 0x42 or 0x43)\n     TMP007 IR Temperature sensor (0x40 - 0x47)\n      TMP006 IR Temperature sensor (0x40 - 0x47)\n      INA219 High-Side DC Current/Voltage sensor (0x40 - 0x4F)  ";
  }   
  if (hexAddress == "0x43") {
    response = "  HDC1008 Humidity/Temp sensor (0x40, 0x41, 0x42 or 0x43)\n     TMP007 IR Temperature sensor (0x40 - 0x47)\n      TMP006 IR Temperature sensor (0x40 - 0x47)\n      INA219 High-Side DC Current/Voltage sensor (0x40 - 0x4F)  ";
  }   
  if (hexAddress == "0x44") {
    response = "  SHT31 Humidity/Temp sensor (0x44 or 0x45 selectable)\n      TMP007 IR Temperature sensor (0x40 - 0x47)\n      TMP006 IR Temperature sensor (0x40 - 0x47)\n      ISL29125 Color Sensor (0x44 only)\n     INA219 High-Side DC Current/Voltage sensor (0x40 - 0x4F)\n      STMPE610/STMPE811 Resistive Touch controller (0x41 or 0x44) ";
  }   
  if (hexAddress == "0x45") {
    response = "  SHT31 Humidity/Temp sensor (0x44 or 0x45 selectable)\n      TMP007 IR Temperature sensor (0x40 - 0x47)\n      TMP006 IR Temperature sensor (0x40 - 0x47)\n      INA219 High-Side DC Current/Voltage sensor (0x40 - 0x4F)  ";
  }   
  if (hexAddress == "0x46") {
    response = "  TMP007 IR Temperature sensor (0x40 - 0x47)\n      TMP006 IR Temperature sensor (0x40 - 0x47)\n      INA219 High-Side DC Current/Voltage sensor (0x40 - 0x4F)  ";
  }   
  if (hexAddress == "0x47") {
    response = "  TMP007 IR Temperature sensor (0x40 - 0x47)\n      TMP006 IR Temperature sensor (0x40 - 0x47)\n      INA219 High-Side DC Current/Voltage sensor (0x40 - 0x4F)  ";
  }   
  if (hexAddress == "0x48") {
    response = "  TMP102 Temperature sensor (0x48 0x49 0x4A or 0x4B)\n      PN532 NFC/RFID reader (0x48 only)\n     ADS1115 4-channel 16-bit ADC (0x48 0x49 0x4A or 0x4B)\n     INA219 High-Side DC Current/Voltage sensor (0x40 - 0x4F)  ";
  }   
  if (hexAddress == "0x49") {
    response = "  TSL2561 light sensor (0x29, 0x39 or 0x49)\n     TMP102 Temperature sensor (0x48 0x49 0x4A or 0x4B)\n      ADS1115 4-channel 16-bit ADC (0x48 0x49 0x4A or 0x4B)\n     INA219 High-Side DC Current/Voltage sensor (0x40 - 0x4F)  ";
  }   
  if (hexAddress == "0x4A") {
    response = "  TMP102 Temperature sensor (0x48 0x49 0x4A or 0x4B)\n      ADS1115 4-channel 16-bit ADC (0x48 0x49 0x4A or 0x4B)\n     INA219 High-Side DC Current/Voltage sensor (0x40 - 0x4F)  ";
  }   
  if (hexAddress == "0x4B") {
    response = "  TMP102 Temperature sensor (0x48 0x49 0x4A or 0x4B)\n      ADS1115 4-channel 16-bit ADC (0x48 0x49 0x4A or 0x4B)\n     INA219 High-Side DC Current/Voltage sensor (0x40 - 0x4F)  ";
  }   
  if (hexAddress == "0x4C") {
    response = "  INA219 High-Side DC Current/Voltage sensor (0x40 - 0x4F)  ";
  }   
  if (hexAddress == "0x4D") {
    response = "  INA219 High-Side DC Current/Voltage sensor (0x40 - 0x4F)  ";
  }   
  if (hexAddress == "0x4E") {
    response = "  INA219 High-Side DC Current/Voltage sensor (0x40 - 0x4F)  ";
  }   
  if (hexAddress == "0x4F") {
    response = "  INA219 High-Side DC Current/Voltage sensor (0x40 - 0x4F)  ";
  }   
  if (hexAddress == "0x50") {
    response = "  MB85RC I2C FRAM (0x50 - 0x57) ";
  }   
  if (hexAddress == "0x51") {
    response = "  MB85RC I2C FRAM (0x50 - 0x57) ";
  }   
  if (hexAddress == "0x52") {
    response = "  MB85RC I2C FRAM (0x50 - 0x57)\n     Nintendo Nunchuck controller (0x52 only)  ";
  }   
  if (hexAddress == "0x53") {
    response = "  ADXL345 3-axis accelerometer (0x1D or 0x53)\n     MB85RC I2C FRAM (0x50 - 0x57) ";
  }   
  if (hexAddress == "0x54") {
    response = "  MB85RC I2C FRAM (0x50 - 0x57) ";
  }   
  if (hexAddress == "0x55") {
    response = "  MB85RC I2C FRAM (0x50 - 0x57) ";
  }   
  if (hexAddress == "0x56") {
    response = "  MB85RC I2C FRAM (0x50 - 0x57) ";
  }   
  if (hexAddress == "0x57") {
    response = "  MB85RC I2C FRAM (0x50 - 0x57)\n     MAX3010x Pulse & Oximetry sensor (0x57) ";
  }   
  if (hexAddress == "0x58") {
    response = "  TPA2016 I2C-controlled Amplifier (0x58 only)\n      SGP30 Gas Sensor (0x58 only)  ";
  }   
  if (hexAddress == "0x5A") {
    response = "  MPR121 12-point capacitive touch sensor (0x5A, 0x5B, 0x5C, 0x5D)\n      CCS811 VOC sensor (0x5A or 0x5B)\n      MLX9061x IR temperature sensor (0x5A only)\n      DRV2605 Haptic Motor Driver (0x5A only) ";
  }   
  if (hexAddress == "0x5B") {
    response = "  MPR121 12-point capacitive touch sensor (0x5A, 0x5B, 0x5C, 0x5D)\n      CCS811 VOC sensor (0x5A or 0x5B)  ";
  }   
  if (hexAddress == "0x5C") {
    response = "  AM2315 Humidity/Temp sensor (0x5C only)\n     MPR121 12-point capacitive touch sensor (0x5A, 0x5B, 0x5C, 0x5D)  ";
  }   
  if (hexAddress == "0x5D") {
    response = "  MPR121 12-point capacitive touch sensor (0x5A, 0x5B, 0x5C, 0x5D)  ";
  }   
  if (hexAddress == "0x60") {
    response = "  MPL115A2 Barometric Pressure (0x60 only)\n      MPL3115A2 Barometric Pressure (0x60 only)\n     Si5351A Clock Generator (0x60 or 0x61)\n      Si1145 Light/IR Sensor (0x60 only)\n      MCP4725A0 12-bit DAC (0x60 or 0x61)\n     TEA5767 Radio receiver (0x60 only)  ";
  }   
  if (hexAddress == "0x61") {
    response = "  Si5351A Clock Generator (0x60 or 0x61)\n      MCP4725A0 12-bit DAC (0x60 or 0x61) ";
  }   
  if (hexAddress == "0x62") {
    response = "  MCP4725A1 12-bit DAC (0x62 or 0x63) ";
  }   
  if (hexAddress == "0x63") {
    response = "  MCP4725A1 12-bit DAC (0x62 or 0x63)\n     Si4713 FM Transmitter with RDS (0x11 or 0x63) ";
  }   
  if (hexAddress == "0x64") {
    response = "  MCP4725A2 12-bit DAC (0x64 or 0x65) ";
  }   
  if (hexAddress == "0x65") {
    response = "  MCP4725A2 12-bit DAC (0x64 or 0x65) ";
  }   
  if (hexAddress == "0x66") {
    response = "  MCP4725A3 12-bit DAC (0x66 or 0x67) ";
  }   
  if (hexAddress == "0x67") {
    response = "  MCP4725A3 12-bit DAC (0x66 or 0x67) ";
  }   
  if (hexAddress == "0x68") {
    response = "  AMG8833 IR Thermal Camera Breakout (0x68 or 0x69)\n     DS1307 RTC (0x68 only)\n      PCF8523 RTC (0x68 only)\n     DS3231 RTC (0x68 only)\n      MPU-9250 9-DoF IMU (0x68 or 0x69)\n     MPU-60X0 Accel+Gyro (0x68 or 0x69)\n      ITG3200 Gyro (0x68 or 0x69) ";
  }   
  if (hexAddress == "0x69") {
    response = "  AMG8833 IR Thermal Camera Breakout (0x68 or 0x69)\n     MPU-9250 (0x68 or 0x69)\n     MPU-60X0 Accel+Gyro (0x68 or 0x69)\n      ITG3200 Gyro (0x68 or 0x69) ";
  }   
  if (hexAddress == "0x6A") {
    response = "  L3GD20H gyroscope (0x6A or 0x6B)\n      LSM9DS0 9-axis IMU (0x1D or 0x1E for Accel/Mag, 0x6A or 0x6B for Gyro)  ";
  }   
  if (hexAddress == "0x6B") {
    response = "  L3GD20H gyroscope (0x6A or 0x6B)\n      LSM9DS0 9-axis IMU (0x1D or 0x1E for Accel/Mag, 0x6A or 0x6B for Gyro)  ";
  }   
  if (hexAddress == "0x70") {
    response = "  TCA9548 1-to-8 I2C Multiplexer (0x70 - 0x77)\n      HT16K33 LED Matrix Driver (0x70 - 0x77) ";
  }   
  if (hexAddress == "0x71") {
    response = "  TCA9548 1-to-8 I2C Multiplexer (0x70 - 0x77)\n      HT16K33 LED Matrix Driver (0x70 - 0x77) ";
  }   
  if (hexAddress == "0x72") {
    response = "  TCA9548 1-to-8 I2C Multiplexer (0x70 - 0x77)\n      HT16K33 LED Matrix Driver (0x70 - 0x77) ";
  }   
  if (hexAddress == "0x73") {
    response = "  TCA9548 1-to-8 I2C Multiplexer (0x70 - 0x77)\n      HT16K33 LED Matrix Driver (0x70 - 0x77) ";
  }   
  if (hexAddress == "0x74") {
    response = "  IS31FL3731 144-LED CharliePlex driver (0x74 0x75 0x66 or 0x77)\n      TCA9548 1-to-8 I2C Multiplexer (0x70 - 0x77)\n      HT16K33 LED Matrix Driver (0x70 - 0x77) ";
  }   
  if (hexAddress == "0x75") {
    response = "  IS31FL3731 144-LED CharliePlex driver (0x74 0x75 0x66 or 0x77)\n      TCA9548 1-to-8 I2C Multiplexer (0x70 - 0x77)\n      HT16K33 LED Matrix Driver (0x70 - 0x77) ";
  }   
  if (hexAddress == "0x76") {
    response = "  BME280 Temp/Barometric/Humidity (0x76 or 0x77)\n      BMP280 Temp/Barometric (0x76 or 0x77)\n     IS31FL3731 144-LED CharliePlex driver (0x74 0x75 0x66 or 0x77)\n      TCA9548 1-to-8 I2C Multiplexer (0x70 - 0x77)\n      HT16K33 LED Matrix Driver (0x70 - 0x77)\n     MS5607/MS5611 Barometric Pressure (0x76 or 0x77)  ";
  }
  if (hexAddress == "0x77") {
    response = "  BME280 Temp/Barometric/Humidity (0x76 or 0x77)\n      BMP280 Temp/Barometric (0x76 or 0x77)\n     BMP180 Temp/Barometric (0x77 only)\n      BMP085 Temp/Barometric (0x77 only)\n      TCA9548 1-to-8 I2C Multiplexer (0x70 - 0x77)\n      IS31FL3731 144-LED CharliePlex driver (0x74 0x75 0x66 or 0x77)\n      HT16K33 LED Matrix Driver (0x70 - 0x77)\n     BMA180 Accelerometer (0x77 only)\n      MS5607/MS5611 Barometric Pressure (0x76 or 0x77)  ";
  }

  return response;
  
}

